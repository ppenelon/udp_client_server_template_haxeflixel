package server.messages;

import haxe.io.Input;
import haxe.io.Output;

class Message{
    public static var type:Int = 1;

    public var x:Int;
    public var y:Int;

    public function writeInOutput(output: Output): Void{
        output.writeInt8(type);
        output.writeInt32(this.x);
        output.writeInt32(this.y);
    }

    public function toString(){
        return "Message:{x=" + this.x + ", y=" + this.y + "}";
    }

    public function new(x: Int, y: Int){
        this.x = x;
        this.y = y;
    }

    public static function fromInput(input: Input){
        return new Message(
            input.readInt32(),
            input.readInt32()
        );
    }
}