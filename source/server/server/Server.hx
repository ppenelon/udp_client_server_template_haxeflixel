package server.server;

import sys.net.Host;
import sys.net.UdpSocket;

import server.messages.Message;

class Server{
    
    public static var udp:UdpSocket;
    public static var host:Host;

    public static var x:Int;
    public static var y:Int;

    static public function main():Void{
        x = 0;
        y = 0;

        udp = new UdpSocket();
        host = new Host(Host.localhost());

        //Handle data
        udp.connect(host, 30627);        
        while(true){
            new Message(x, y).writeInOutput(udp.output);
            udp.output.flush();
            x = (x + Std.random(4) + 1) % 500;
            y = (y + Std.random(4) + 1) % 500;
            Sys.sleep(.2);
        }
        udp.close();
    }
}