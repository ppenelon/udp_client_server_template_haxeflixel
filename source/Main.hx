package;

import flixel.FlxG;
import flixel.FlxGame;
import openfl.display.Sprite;

class Main extends Sprite
{
	public function new(){
		super();

		//No splashscreen, no HaxeFlixel mouse and no pause when window isn't active
		addChild(new FlxGame(0, 0, PlayState, 1, 60, 60, true, false));
		FlxG.mouse.useSystemCursor = true;
		FlxG.autoPause = false;
	}
}
