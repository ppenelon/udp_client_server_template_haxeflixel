package;

import flixel.FlxState;
import flixel.FlxSprite;
import flixel.util.FlxColor;

import sys.net.UdpSocket;
import sys.net.Host;

import cpp.vm.Thread;

import server.messages.Message;

class PlayState extends FlxState{

	//Sprite player
	var player:FlxSprite;

	//UDP
	var udp:UdpSocket;
	var host:Host;
	var threadUDP:Thread;

	override public function create():Void{
		//Player sprite
		player = new FlxSprite();
		player.makeGraphic(20, 20, FlxColor.CYAN);
		add(player);

		//UDP Client
		udp = new UdpSocket();
		host = new Host(Host.localhost());
		udp.bind(host, 30627);
		threadUDP = Thread.create(listenNet);

		super.create();
	}

	override public function update(elapsed:Float):Void{		
		super.update(elapsed);
	}

	public function listenNet():Void{
		while(true){
			if(udp.input.readInt8() == Message.type){
				var receivedMessage:Message = Message.fromInput(udp.input);
				player.x = receivedMessage.x;
				player.y = receivedMessage.y;
			}
		}
	}
}
