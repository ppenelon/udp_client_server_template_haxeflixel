
## UDP Client/Server template for HaxeFlixel Game
### Introduction
The notions here are really simple.
We need a **shared library** between client and server so every one can talk the same langage when it's for network communication.
This shared library contains **network message models**.

### Hierarchy
 - **.vscode** : Visual Studio Code projet configuration
 - **assets** : Game ressources folder
 - **source** : Client sources root folder
	 - **server** : Server sources and network message models
		 - **messages** : Network message models
		 - **server** : Server sources root folder 

### Get vscode code completion
Jump to the root (where Project.xml is) and run this command :

    lime build flash
You will probably have an error but the code completion will be available anyway.

### Server compilation
To compile the server, jump to the ***/source*** folder (where the messages and server folders are) and run this command :

    haxe -main server.server.Server -cpp ../bin
Then you can locate the server at the ***/bin*** folder.

### Client compilation
To compile it, jump to the root and run this command :

    lime build windows
Then you can locate the client at the ***/export/windows/cpp/bin*** folder.  
If you want to build and run the project jump to the root and run this command :

    lime test windows

### What to know ?
 1. When you create a class, put his package at the beginning of the file. For example, if you create a Server class here : ***/source/server/server***. Add this line at the beggining of the file : `package server.server;`
 2. A class created at the root folder have this package : `package;`
 3. The compilation process compile only the changes.
 
### Problems ?
If you have a problem like this one :

    Error: Source path "D:\HaxeToolkit\haxe\lib\hxcpp/3,4,188/bin/Windows/std.dll" does not exist
Run this command :

    haxelib set hxcpp 3.4.64